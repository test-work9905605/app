FROM node:21-alpine AS builder

# Establishing working directory
WORKDIR /test-admin

# Copying package.json и yarn.lock and install dependencies
COPY ./test-admin .
RUN yarn install

# Copying all files to current directory
# COPY ./test-admin/src ./test-admin/public ./test-admin/.env ./

# Running tests
# RUN yarn test

# Build app for production
RUN yarn build

# Use ligtweight image for production
FROM nginx:alpine

# Copying static files from dist
COPY --from=builder /test-admin/dist/ /usr/share/nginx/html

# Expose 80
EXPOSE 80

# Running nginx
CMD ["nginx", "-g", "daemon off;"]
